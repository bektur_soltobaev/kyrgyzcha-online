from django.urls import path
from .views import LessonListView

from .views import(
LessonListView,
LessonUpdateView,
LessonDetailView,
LessonDeleteView, 
LessonCreateView,
)

urlpatterns = [
    path('<int:pk>/edit/', LessonUpdateView.as_view(), name='lesson_edit'), 
    path('<int:pk>/', LessonDetailView.as_view(), name='lesson_detail'), 
    path('<int:pk>/delete/', LessonDeleteView.as_view(), name='lesson_delete'), 
    path('new/', LessonCreateView.as_view(), name='lesson_new'),
    path('', LessonListView.as_view(), name='lesson_list'),
]