from django.views.generic import ListView, DetailView 
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.urls import reverse_lazy
from .models import Lesson

class LessonListView(ListView):
    model = Lesson
    template_name = 'lesson_list.html'

class LessonDetailView(DetailView): 
    model = Lesson
    template_name = 'lesson_detail.html'

class LessonUpdateView(UpdateView): 
    model = Lesson
    fields = ('title', 'body',)
    template_name = 'lesson_edit.html'
    
class LessonDeleteView(DeleteView): 
    model = Lesson
    template_name = 'lesson_delete.html'
    success_url = reverse_lazy('lesson_list')

class LessonCreateView(CreateView):
    model = Lesson
    template_name = 'lesson_new.html'
    fields = ('title', 'body', )
