from django.db import models
from accounts.models import CustomUser, Profile

GRADES = (
    (1, '1'),
    (2, '2'),
    (3, '3'),
    (4, '4'),
    (5, '5'),
)

class Lesson(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    teacher = models.OneToOneField(Profile, on_delete=models.CASCADE, related_name='lesson_to_teach')
    student = models.OneToOneField(Profile, on_delete=models.CASCADE, related_name='lesson_to_take')
    rating = models.IntegerField(choices=GRADES, default=5)
    start_time = models.DateTimeField(auto_now_add=True)

    def get_absolute_url(self):
        return reverse('lesson_detail', args=(self.id, ))

    def __str__(self):
        return self.title