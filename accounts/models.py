from django.contrib.auth.models import AbstractUser
from multiselectfield import MultiSelectField
from django.db import models

HOURS = (
    (1, '9 am'),
    (2, '10 am'),
    (3, '11 am'),
    (4, '1 pm'),
    (5, '2 pm'),
    (6, '3 pm'),
    (7, '4 pm'),
    (8, '5 pm'),
)

LEVEL = (
    (1, 'Beginner'),
    (2, 'Intermediate'),
    (3, 'Advanced'),
)

class CustomUser(AbstractUser):
    is_student = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)

class Profile(models.Model):
    user = models.OneToOneField(AbstractUser, on_delete=models.CASCADE, related_name='profile')
    full_name = models.CharField(max_length=50)
    knowledge_level = models.IntegerField(choices=LEVEL)
    # available_hours = models.MultiSelectField(choices=HOURS)
    skype_account = models.CharField(max_length=16)
    phone_number = models.CharField(max_length=16)
    tuition = models.ManyToManyField("self", blank=True)
    

    def __str__(self):
        return self.user.username
