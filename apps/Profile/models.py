from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.utils import timezone



class User(AbstractBaseUser):
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=12)
    is_teacher = models.BooleanField(default=False)
    is_student = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'

    def __str__(self):
        return self.email

class Profile(models.Model):
    LEVEL = {
        (1, 'Базовый'),
        (2, 'Средний'),
        (3, 'Продвинутый'),
    }

    TIMES = {
        (1, 'с 7:00 - до 8:00'),
        (2, 'с 8:00 - до 9:00'),
        (3, 'с 9:00 - до 10:00'),
        (4, 'с 11:00 - до 12:00'),
        (5, 'с 12:00 - до 13:00'),
        (6, 'с 13:00 - до 14:00'),
        (7, 'с 14:00 - до 15:00'),
        (8, 'с 15:00 - до 16:00'),
        (9, 'с 16:00 - до 17:00'),
        (10, 'с 17:00 - до 18:00'),
        (11, 'с 18:00 - до 19:00'),
        (12, 'с 19:00 - до 20:00'),
        (13, 'с 21:00 - до 22:00'),
    }

    user = models.OneToOneField(User, related_name='profile', on_delete=models.CASCADE)
    full_name = models.CharField(max_length=200, blank=False, null=True)
    skype_account = models.CharField(max_length=20, blank=False, null=True)
    level = models.IntegerField(choices=LEVEL, blank=False, null=True, default=1)
    date_joined = models.DateTimeField(default=timezone.now)
    phone_number = models.CharField(max_length=20, help_text='Введите ваш номер:', unique=True)
    profandprof = models.ManyToManyField('Profile')
    comfortable_time = models.IntegerField(choices=TIMES, default=1)

    def __str__(self):
        return self.full_name
