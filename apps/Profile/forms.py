from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import User, Profile


class UserCreationForm(UserCreationForm):

    class Meta:
        model = Profile
        exclude = ('profandprof',)


class UserChangeForm(UserChangeForm):

    class Meta:
        model = User
        fields = ('email',)


class TeacherCreateForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = Profile

    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_teacher = True
        if commit:
            user.save()
        return user
