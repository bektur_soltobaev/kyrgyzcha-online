from django.shortcuts import render
from django.views.generic import CreateView
from .models import Profile, User
from .forms import TeacherCreateForm


class TeacherCreateView(CreateView):
    model = Profile
    form_class = TeacherCreateForm
    template_name = 'teacher_create.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'teacher'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('teacher_create.html')       
