from django.urls import path
from .views import TeacherCreateView


urlpatterns = [
    path('', TeacherCreateView.as_view(), name='signup_teacher')
]
